#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 30 09:44:43 2017

@author: kimadmin
"""

#Goal of script: find the rosetta_project folder, then perform operations to
#make a clean pdb file and a clean resfile

#IMPORT RELEVANT MODULES
import os
import sys


dirName = 'rosetta_project'

for root, dir, files, in os.walk('/home/kimlab2/owenwhitley/'): #edited 6.5.17 to change root dir
    if dirName in dir:
        rosetta_proj_path = (os.path.join(root,dirName))
        break

#==============================================================================
#SETUP WORKING DIRECTORY, A BUNCH OF OTHER DIRECTORIES TO FIND CLEANINGUTILS,
#FINDFILES, RAW PDBS
os.chdir(rosetta_proj_path) #working directory is now 
rosetta_utils_path = rosetta_proj_path + "/rosetta_python_utils"    
pdbPath = rosetta_proj_path + "/raw_pdbs" #can be later modified to take any folder

#==============================================================================
#Append paths to sys.path so that appropriate modules can be imported
sys.path.append(rosetta_utils_path)
import findFiles
import clean_pdb
import resFileUtilsV2

#==============================================================================
#new directories
cleanPDB_dir = rosetta_proj_path + "/clean_pdbs" #Will need to modify this so that
                                            #unique folders can be made
raw_resfile_dir = rosetta_proj_path + "/raw_resfile"

#create two lists of directories. if you already have an existing directory,
#don't make another one
cleanPDBdirs = findFiles.find_directory("clean_pdbs", rosetta_proj_path, 
                              returnFirst = False)
raw_res_dirs = findFiles.find_directory("raw_resfile", rosetta_proj_path, 
                              returnFirst = False)
#If above directories for clean PDB and raw resfile do not already exist, make them
                              
if len(cleanPDBdirs) == 0:
    os.mkdir(cleanPDB_dir)
if len(raw_res_dirs) == 0:
    os.mkdir(raw_resfile_dir)
#==============================================================================
#get a list of pdb files to clean and generate resfile from
pdbList = findFiles.return_filelist(pdbPath)


for i in range(0,len(pdbList[0]),1):
    
    #create a clean pdb file and a basic resfile for each 
    
    pdbFullPath = pdbPath + "/" + pdbList[0][i]
    
    clean_pdb.clean_pdb(filePath = pdbFullPath, keepLines = ["ATOM","TER"], 
                        suffix = "_clean", destPath = cleanPDB_dir,
                        overwrite = True)
    resFileUtilsV2.make_resfile(filePath = pdbFullPath, suffix = ".resfile",
             destPath = raw_resfile_dir, overwrite = True, UserInput_SC = True,
            allowables = ["ATOM"])
    
    
    
    
    


    