#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 09:18:08 2017

@author: owenwhitley
"""

#purpose of script: provide a command line interface by which to call for
#execution of pdb to iterative mutation pipeline

import sys
import argparse
import os

parser = argparse.ArgumentParser(prog = "KimLab Python-Rosetta Iterative Mutant Structure Generation",
                        description = '''
iteratively generate mutant structures via rosetta program.
                  
  Given a pdb file, a list of residues to mutate,
  and a list of non-canonical amino acids, generate
  a PDB structure with a given non-canonical amino acid
  replacing a given residue for all non-canonical amino acids
  and for all residues.
                  
                  ''')

parser.add_argument('-in','--input_directory', required = True, help = 'folder containing all input files')
parser.add_argument('-out','--output_directory', required = True,
                    help = 'destination for all output. subfolders wil be made to contain output of specific\
functions used in this python program')
parser.add_argument('--rosetta_scripts_exe', required = True,
                    help = 'executable for rosetta scripts')
parser.add_argument('--rosetta_python_dir', required = True, 
                    help = 'top directory for folders containing kimlab rosetta\
python utilities and scripts')
parser.add_argument('--relax_protocol', 
                    help = 'xml file with instructions for rosetta scripts\
if unspecified defaults to rosetta_fast_relax.xml')
parser.add_argument('--pack_protocol',
                    help = 'xml file with instructions for rosetta scripts\
if unspecified defaults to rosettaPack.xml')
parser.add_argument('-rc','--relax_commands', action = 'append',
                    help = 'commands given to rosetta when generating a relaxed\
pdb structure. commands will be applied to all residues. Commands written to resfile\
  in the columns after residue and chain specified. Note that each command must\
  be specified individually by a -rc flag. If no commands specified, the default\
  for this option is NATAA EX 1 EX 2. Example usage: -rc \'NATAA\' \
  -rc \'EX 1\' gives commands NATAA EX 1.')
parser.add_argument('-pc','--pack_commands', action = 'append', help =
                    'commands applied to all residues (except mutated residue)\
 when going through a packing protocol in rosetta. Commands written to resfile\
  in the columns after residue and chain specified. Note that each command must\
  be specified individually by a -pc flag. If no commands specified, the default\
  for this option is NATAA EX 1 EX 2. Example usage: -pc \'NATAA\' \
  -pc \'EX 1\' gives commands NATAA EX 1.')
parser.add_argument('-mc', '--mut_res_commands', action = 'append', help = 
 'commands given to rosetta when designing and packing a mutant structure. \
 if left unspecified, defaults to EX 1 EX 2. The residue to be mutated is \
 altered in the resfile to have the commands EMPTY NC NCAA_ID (one of the ncaas\
 from your list of ncaas) and any commands from\
 mc will be appended to those commands. Note that these commands only apply to \
 the mutant residue. Also note that commands must be specified individually \
 with individual mc commands. E.g., to append EX 1 EX 2 to the mutant commands\
  you would type -mc \'EX 1\' -mc \'EX 2\'')
parser.add_argument('--ncaa_list', help = 
'list of NCAAs that will be substituted at specified residue positions.\
 if unspecified, will default to NCAA_list.txt in $rosetta_python_folder/NCAA_list.\
 Note that all NCAAs MUST have existing rotlib files in \
  $PATH_TO_ROSETTA/main/database/rotamer/ncaa_rotlibs/ncaa_rotamer_libraries/alpha_amino_acid')
parser.add_argument('--raw_pdb', required = True, help = 'pdb file on which to \
 perform design, pulled straight from RSCB. This PDB file not edited, rather \
 structures generated using this pdb file as a template')
parser.add_argument('--residue_list', required = True, help = 
                    'list of residues, with each row formatted as\
residue # <whitespace> chain ID.')

args = parser.parse_args()

input_dir = args.input_directory
output_dir = args.output_directory
rosetta_scripts_exe = args.rosetta_scripts_exe
    
#Add rosetta_python_utils folder to the path and import some of the utils
rosetta_python_path = args.rosetta_python_dir
rosetta_py_utils_path = os.path.join(rosetta_python_path, 'rosetta_python_utils')
sys.path.append(rosetta_py_utils_path)

import parseText as PT #This will be used downstream to parse residue_list 



relax_protocol = args.relax_protocol
pack_protocol = args.pack_protocol
ncaa_list = args.ncaa_list
raw_pdb = args.raw_pdb
residue_list_file = args.residue_list
relax_commands = args.relax_commands
pack_commands = args.pack_commands
mut_res_commands = args.mut_res_commands

#Setting defaults for relax and pack protocols and commands

if args.relax_protocol == None:
    xml_folder1 = os.path.join(rosetta_python_path,"rosetta_scripts_xmls")
    relax_protocol = os.path.join(xml_folder1, 'rosetta_fast_relax.xml')

if args.pack_protocol == None:
    xml_folder2 = os.path.join(rosetta_python_path,"rosetta_scripts_xmls")
    pack_protocol = os.path.join(xml_folder2, 'rosettaPack.xml')
    
if ncaa_list == None:
    ncaa_folder = os.path.join(rosetta_python_path, 'NCAA_list')
    ncaa_list_filepath = os.path.join(ncaa_folder, 'NCAA_list.txt')
else:
    ncaa_list_filepath = os.path.join(input_dir, ncaa_list)
    
    if os.path.exists(ncaa_list_filepath) == False:
        raise ValueError(ncaa_list_filepath + ' does not exist')
    
if relax_commands == None:
    relax_commands = ['NATAA', 'EX 1', 'EX 2']

if pack_commands == None:
    pack_commands = ['NATAA', 'EX 1', 'EX 2']
    
if mut_res_commands == None:
    mut_res_commands = ['EX 1', 'EX 2']
    



#==============================================================================
#CLEAN PDB
import clean_pdb #import function from module clean_pdb

pdb_path = os.path.join(input_dir,raw_pdb)

clean_pdb.clean_pdb( filePath = pdb_path, #path to pdb file
                     keepLines = ["ATOM","TER"], #lines in pdb to keep
                     suffix = "_clean",
                     parent_dir = output_dir, #parent directory of destination folder
                     destPath = 'cleaned_pdb', #name of destination folder
                     overwrite = False, #overwrite existing file?
                     new_directory = True, 
                     logfile = True)

#==============================================================================
#Make the resfile

import resFileUtilsV2 as RFU

clean_pdb_folder = os.path.join(output_dir, 'cleaned_pdb')
clean_pdb_name = os.listdir(clean_pdb_folder)[0] #retrieves name of clean pdb file
clean_pdb_path = os.path.join(clean_pdb_folder, clean_pdb_name)
raw_resfile_name = clean_pdb_name[0:len(clean_pdb_name)-4] + '.resfile'

RFU.make_resfile(pdbPath = clean_pdb_path, #filepath to pdb file you wish to generate resfile for
                 resfile_name = raw_resfile_name, #suffix for file
                 destPath = "raw_resfile", #enter a string here for the destination folder (not a full directory path)
                 parent_directory = output_dir, #parent directory of destination
                 new_directory = True, #boolean. Do we make a new directory?
                 overwrite = False, #boolean. do we overwrite an existing resfile?
                 Use_Input_SC = True, #boolean. If true, will write USER_INPUT_SC to first line
                                     #meaning that rosetta will allow experimentally determined rotamers
                                     #in the output structure it generates (if using packer)
                 allowables = ["ATOM", "TER"], #which lines do we allow? format as a list with string elements
                 logfile = True, #Do we write a logfile?
                 logIndicator = "make_resfile")
    
#==============================================================================
#Make two modded resfiles, such that there exist a set of commands for relaxation
#and a set of commands for packing and mutation (i.e. a resfile for relax protocol
#and a refile for the pack protocol)

raw_resfile_folder = os.path.join(output_dir, 'raw_resfile')
raw_resfile = os.path.join(raw_resfile_folder, raw_resfile_name)
relax_resfile_name = raw_resfile_name[0:-9] + '_relax_mod.resfile'
pack_resfile_name = raw_resfile_name[0:-9] + '_pack_mod.resfile'

RFU.edit_resfile(source_resfile = raw_resfile, 
                 output_dir = os.path.join(output_dir, 'modded_resfiles'), 
                 new_directory = True, 
                 fName = relax_resfile_name, 
                 overwrite_resfile = False,
                 edit_mode = 'chains', 
                 chains = ['ALL'], 
                 position = None,
                 AAstart = None, 
                 AAend = None, 
                 Commands = relax_commands,
                 logfile = True, 
                 logfile_name = relax_resfile_name + '.logfile',
                 logfile_append = False,
                 logIndicator = "relax_resfile")

RFU.edit_resfile(source_resfile = raw_resfile, 
                 output_dir = os.path.join(output_dir, 'modded_resfiles'), 
                 new_directory = False, 
                 fName = pack_resfile_name, 
                 overwrite_resfile = False,
                 edit_mode = 'chains', 
                 chains = ['ALL'], 
                 position = None,
                 AAstart = None, 
                 AAend = None, 
                 Commands = pack_commands,
                 logfile = True, 
                 logfile_name = pack_resfile_name + '.logfile',
                 logfile_append = False,
                 logIndicator = "pack_resfile")

#==============================================================================
#Generate a relaxed pdb

import rosetta_interfaces as RI
print(os.getcwd())
modded_resfile_folder = os.path.join(output_dir, 'modded_resfiles')
relaxed_resfile_path = os.path.join(modded_resfile_folder, relax_resfile_name)

RI.relax_pdb(clean_pdb_path, 
              relaxed_resfile_path, 
              rosetta_scripts_exe,
              relax_protocol, 
              clean_pdb_name[0:-4] + '_relaxed', 
              output_dir)

#==============================================================================
#From relaxed pdb, make a whole bunch of structures

relaxed_pdb_folder = os.path.join(output_dir, clean_pdb_name[0:-4] + '_relaxed') #here folder refers to an entire path to a directory
relaxed_pdb_name = clean_pdb_name[0:-4]+"_0001.pdb"
relaxed_pdb_path = os.path.join(relaxed_pdb_folder, relaxed_pdb_name)
mutate_pack_output_folder = clean_pdb_name[0:-4] + "_mutate_and_pack"

pack_resfile_path = os.path.join(modded_resfile_folder, pack_resfile_name)

residue_list_path = os.path.join(input_dir, residue_list_file)

residue_list_fileObj = open(residue_list_path, 'r')

residue_list = PT.parseText2(residue_list_fileObj)

#below: an ugly way to make sure everything in column 1 of the residue list is of type integer
for i in range(0,len(residue_list),1):
    residue_list [i][0] = int(residue_list[i][0])



RI.mutate_and_pack(relaxed_pdb_path, 
                    rosetta_scripts_exe,
                    pack_protocol,
                    mutate_pack_output_folder, 
                    output_dir,
                    pack_resfile_path, 
                    ncaa_list_filepath, 
                    residue_list, #List of residues to be mutated
                    mut_res_commands, #to be appended to EMPTY NC in commands for mutant residues
                    "", #suffix, for naming files/folders
                    True)

#==============================================================================
#Retrieve scores:

mutant_folder_fullpath = os.path.join(output_dir, mutate_pack_output_folder)

RI.make_scoretable(mutant_folder_fullpath, overwrite = False)