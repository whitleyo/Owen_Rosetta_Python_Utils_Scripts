#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 09:50:24 2017

@author: kimadmin
"""
#GOAL OF SCRIPT: Score a PDB, then relax it. If the relaxing works, we should
#get a better rosettascore

#IMPORT OS AND SYS
import os
import sys


dirName = 'rosetta_project'

for root, dir, files, in os.walk('/home/kimlab2/owenwhitley/'): #edited 6.5.17 to change root dir
    if dirName in dir:
        rosetta_proj_path = (os.path.join(root,dirName))
        break
    
#==============================================================================
# SETUP WORKING DIRECTORY, A BUNCH OF OTHER DIRECTORIES TO FIND UTILS AND
# RESFILES
os.chdir(rosetta_proj_path) #working directory is now 
rosetta_utils_path = rosetta_proj_path + "/rosetta_python_utils"
resfile_path = rosetta_proj_path + "/modded_resfile"

sys.path.append(rosetta_utils_path)

#==============================================================================
# IMPORT ALL RELEVANT UTIL MODULES

import findFiles as FF
import resFileUtilsV2 as RFU
import rosetta_interfaces as RI

#Setup filepaths for scoring and relaxing
clean_pdb_filepath = rosetta_proj_path + "/clean_pdbs/2gl7_clean.pdb"
rosetta_scripts_executable = "/home/kimlab2/owenwhitley/rosetta_3.8_2017/main/source/bin/rosetta_scripts.static.linuxgccrelease"
rosetta_scripts_xml_score = "/home/kimlab2/owenwhitley/rosetta_project/rosetta_scripts_xmls/rosettaScore_T2014.xml"
rosetta_scripts_xml_relax = "/home/kimlab2/owenwhitley/rosetta_project/rosetta_scripts_xmls/rosetta_fast_relax.xml"
output_dir = "rosetta_interfaces_test_output_060117"
output_dir_parent = rosetta_proj_path
resfile_filepath = resfile_path + "/2gl7.res_modded.resfile"

RI.score_pdb(clean_pdb_filepath, rosetta_scripts_executable, rosetta_scripts_xml_score,
              output_dir, output_dir_parent)

RI.relax_pdb(clean_pdb_filepath, resfile_filepath, rosetta_scripts_executable,
              rosetta_scripts_xml_relax, output_dir, output_dir_parent)
